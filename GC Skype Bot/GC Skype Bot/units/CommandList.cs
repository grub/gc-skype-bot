﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot.units
{
    public class CommandList : gcMain, Unit
    {
        public Semantics semantics { get; private set; }

        public void InitUnit()
        {
            semantics = new Semantics();
            semantics.enabled = true;
            semantics.name = "Commands List";
            semantics.helpText = "Usage: !commands";
            semantics.triggerPre = "!";
            semantics.trigger = "commands";
            semantics.firstChar = true;
            semantics.timeout = 30;
            semantics.requiredPriv = "vip"; //VIP because the list could be quite large and be abused to fill the chat excessively.
            semantics.deniedPrivs = new string[1] { "banned" };
        }

        public void DoCommand(UserD sender, ChatMessage msg)
        {
            string list = "";
            foreach (Unit unit in skypeHandler.messageHandler.listOfUnits)
            {
                Semantics s = unit.semantics;
                list += "'" + s.name + "' (" + s.triggerPre + s.trigger + "), ";   //e.g. 'Commands List' (!commands), needs 'vip' flag, disallows 'banned' flag.

                if (!string.IsNullOrEmpty(s.requiredPriv))
                {
                    list += "requires '" + s.requiredPriv + "' flag";
                }
                else
                {
                    list += "available to all";
                }

                if (s.deniedPrivs.Length > 0)
                {
                    list += ", disallows '" + s.deniedPrivs[0] + "'";
                    if (s.deniedPrivs.Length > 1)
                    {
                        foreach(string priv in s.deniedPrivs.Skip(1))   //Iterate over all the denied privs to add them all to the list (except the first as it was already added).
                        {
                            list += ", '" + priv + "'";
                        }
                        list += " flags.";
                    }
                    else
                    {
                        list += " flag.";
                    }
                }

                list += Environment.NewLine;
            }

            skypeHandler.SendMessage(list);
        }
    }
}
