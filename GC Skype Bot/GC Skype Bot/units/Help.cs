﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot.units
{
    public class Help : gcMain, Unit
    {
        public Semantics semantics { get; private set; }

        public void InitUnit()
        {
            semantics = new Semantics();
            semantics.enabled = true;
            semantics.name = "Command Helper";
            semantics.helpText = "Usage: !help <command>";
            semantics.triggerPre = "!";
            semantics.trigger = "help";
            semantics.firstChar = true;
            semantics.timeout = 5;
            semantics.requiredPriv = "";
            semantics.deniedPrivs = new string[1] { "banned" };
        }

        public void DoCommand(UserD sender, ChatMessage msg)
        {
            string[] querySplit = msg.Body.Split(' ');
            if (querySplit.Length == 2) //Make sure the help command is being used correctly (i.e. !help rtd, NOT !help, or !help roll the dice).
            {
                foreach (Unit unit in skypeHandler.messageHandler.listOfUnits)
                {
                    if (querySplit[1] == unit.semantics.trigger)
                    {
                        skypeHandler.SendMessage(sender.Handle + ": " + unit.semantics.helpText);   //Send syntax help for the requested command.
                        return;
                    }
                }
            }
            else
            {
                skypeHandler.SendMessage(sender.Handle + ": " + semantics.helpText);    //Send a message to the user if they did not use the help command correctly.
            }
        }
    }
}
