﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot.units
{
    public class RollTheDice : gcMain, Unit
    {
        public Semantics semantics { get; private set; }

        public void InitUnit()
        {
            semantics = new Semantics();
            semantics.enabled = true;
            semantics.name = "Roll The Dice";
            semantics.helpText = "Usage: !rtd";
            semantics.triggerPre = "!";
            semantics.trigger = "rtd";
            semantics.firstChar = true;
            semantics.timeout = 5;
            semantics.requiredPriv = "";
            semantics.deniedPrivs = new string[1] { "banned" };
        }

        public void DoCommand(UserD sender, ChatMessage msg)
        {
            skypeHandler.SendMessage(sender.Handle + " rolled a " + Roll());  //Send the result of Roll() as a Skype Message
        }

        int Roll()
        {
            return new Random().Next(1, 7);         //Return a random value between 1 and 6
        }
    }
}
