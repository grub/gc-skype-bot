﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot.units
{
    public class Quit : gcMain, Unit
    {
        public Semantics semantics { get; private set; }

        public void InitUnit()
        {
            semantics = new Semantics();
            semantics.enabled = true;
            semantics.name = "Quit";
            semantics.helpText = "Usage: !quit";
            semantics.triggerPre = "!";
            semantics.trigger = "quit";
            semantics.firstChar = true;
            semantics.timeout = 5;
            semantics.requiredPriv = "master";
            semantics.deniedPrivs = new string[1] { "banned" };
        }

        public void DoCommand(UserD sender, ChatMessage msg)
        {
            gcMain.Quit();
        }
    }
}
