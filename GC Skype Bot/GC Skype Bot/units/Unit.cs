﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot.units
{
    public interface Unit
    {
        Semantics semantics { get; }
        void InitUnit();
        void DoCommand(UserD sender, ChatMessage msg);
    }

    //Provide a semantics class for units. Units must specify the majority of these attributes.
    public class Semantics
    {
        public bool enabled = true;     //Enable/disable unit.
        public string name = "Unnamed"; //Name of the unit.
        public string helpText;         //Help text for when the user enters !help <trigger>.
        public string triggerPre = "!"; //Prefix (if any) for the trigger.
        public string trigger;          //The trigger to look for (do not include the prefix).
        public bool firstChar = true;   //Is the trigger the first thing in the message, or can it be found anywhere.
        public int timeout = 5;         //Minimum time between using the command in seconds.
        public string requiredPriv;     //Required privilege (user tag) to use the command.
        public string[] deniedPrivs;    //Denied privileges (one or more user tags) that cannot use the command.
    }
}
