﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot.units
{
    //Every unit needs to inherit the class 'gcMain' and the interface 'Unit'.
    public class ExampleUnit : gcMain, Unit
    {
        public Semantics semantics { get; private set; }

        public void InitUnit()
        {
            semantics = new Semantics();
            //Upon initialisation, specify semantic tags for this unit.
            semantics.enabled = true;
            semantics.name = "Example Unit";
            semantics.helpText = "Provide syntax help here";
            semantics.triggerPre = "!";
            semantics.trigger = "trigger";
            semantics.firstChar = true;
            semantics.timeout = 5;
            semantics.requiredPriv = "";
            semantics.deniedPrivs = new string[1] { "banned" };
        }

        public void DoCommand(UserD sender, ChatMessage msg)
        {
            //Code here for when the command gets executed.
        }
    }
}
