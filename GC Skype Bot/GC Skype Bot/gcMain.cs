﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot
{
    public class gcMain
    {
        public static SkypeHandler skypeHandler { get; private set; }

        static void Main(string[] args)
        {
            if (args.Length >= 4)    //If there are an appropriate amount of parameters, allow the code to run
            {
                //Parse the arguments and put the resulting values into the respective variables
                string[] parsedArgs = ParseArgs(args);
                string blob = parsedArgs[0];
                string datadir = parsedArgs[1];

                //Validate the path ends with a backslash and does not use forward slashes but backslashes only.
                if (datadir.EndsWith("\\") && datadir.IndexOf("/") == -1)
                {
                    Logging.path = datadir + "log\\";
                    UserHandler.path = datadir;

                    skypeHandler = new SkypeHandler();

                    if (skypeHandler.SkypeInit(blob))   //If initialised successfully (attach to Skype), do lots of stuff...
                    {
                        UserHandler.Initialise(skypeHandler.skypeChat);
                        Logging.AddToLog("GC Bot Initiated successfully.");
                    }
                    else                                //Otherwise, report a fatal error to the log and finish off...
                    {
                        Logging.AddToLog("FATAL ERROR: Unable to initialise. The bot was likely unable to attach to the Skype process.");
                    }
                }
                else
                {
                    Logging.AddToLog("FATAL ERROR: Invalid path specified. Use backslashes and include a trailing backslash.");
                }
            }
            else                    //Otherwise, if there is not enough parameters, report fatal error and finish off...
            {
                Logging.AddToLog("FATAL ERROR: Not enough paramters. Minimum: -blob <blob> -datadir <path>");
            }
            Console.WriteLine("Press enter at any point to exit...");
            Console.ReadLine();
            Quit();
        }

        public static void Quit()
        {
            if (UserHandler.SaveXML())
            {
                Logging.AddToLog("Saved user XML.");
            }
            else
            {
                Logging.AddToLog("ERROR: Unable to save user XML before quitting. Any changes may have been lost.");
            }
            Logging.AddToLog("Bot closed.");
            Logging.SaveLog();
            Environment.Exit(0);
        }

        private static string[] ParseArgs(string[] args)
        {
            string[] parsed = new string[2];

            //Loop through all the command-line arguments
            for (int i = 0; i < args.Length; i++)
            {
                //Find options (hyphenated)
                if (IsOption(args[i]))
                {
                    //Parse option...
                    switch (args[i])
                    {
                        case "-blob":
                            parsed[0] = GetArgument(args, i);
                            break;

                        case "-datadir":
                            parsed[1] = GetArgument(args, i);
                            break;
                    }
                }
            }

            return parsed;
        }

        //Checks that the specified string is an option [true] (starts with a hyphen) or argument [false] (starts with any character except hythen).
        private static bool IsOption(string text)
        {
            if (!text.StartsWith("-"))  //If it doesn't start with a hyphen,
                return false;               //Return false.

            return true;
        }

        //Overload for IsArg, provides an out value which contains the parameter converted to lowercase.
        private static bool IsOption(string text, out string option)
        {
            option = "";

            if (!IsOption(text))           //If it isn't an option argument,
                return false;               //Return false (and leave param - the out value - as an empty string).

            option = text.ToLower();     //Otherwise set param to the parameter, forced lowercase.
            return true;
        }

        //Returns the argument string correlating to the option, and provides an out value containing one or more of the indexes that the arguments were found in (after the option index).
        private static string GetArgument(string[] args, int optionIndex, out List<int> argIndexes)
        {
            argIndexes = new List<int>();   //Initialise List of the indexes in which the argument was found (i.e. if the argument spans over multiple whitespaces).
            string argument = "";           //Blank string for storing argument.

            //Loop through to create a string of the full argument (as it may span multiple indexes in the array due to whitespace).
            int i = 1;
            while (optionIndex + i < args.Length && !args[optionIndex + i].StartsWith("-"))
            {
                argument += args[optionIndex + i] + " ";
                argIndexes.Add(optionIndex + i);
                i++;
            }

            //Cleanup, remove trailing whitespace off the argument.
            if (argument.EndsWith(" "))
            {
                argument = argument.Substring(0, argument.Length - 1);
            }

            return argument;
        }

        //Overload which does not have an out value of the index(es) where the argument was found.
        private static string GetArgument(string[] args, int optionIndex)
        {
            List<int> tempList = new List<int>();                           //Create new temporary list
            string argument = GetArgument(args, optionIndex, out tempList); //Call the function
            tempList = null;        //Nullify the temporary list as it is not used beyond this point
            return argument;        //Return the argument string
        }
    }

    public class SkypeHandler
    {
        private Skype skype;
        public Chat skypeChat;
        public MessageHandler messageHandler;

        private bool initialised = false;

        public bool SkypeInit(string chatBlob)
        {
            if (!initialised)
            {
                skype = new Skype();
                messageHandler = new MessageHandler();

                //Attempt to attach to the Skype process, and handle an exception which is fired if this is not possible
                try
                {
                    skype.Attach();
                }
                catch (Exception e)
                {
                    return false;
                }

                skypeChat = skype.FindChatUsingBlob(chatBlob);
                messageHandler.skypeChat = skypeChat;
                messageHandler.Initialise();
                skype.MessageStatus += new _ISkypeEvents_MessageStatusEventHandler(messageHandler.OnReceiveMessage);
            }

            initialised = true;
            return true;
        }

        //Overload function
        public bool SkypeInit(Chat chat)
        {
            return SkypeInit(chat.Blob);
        }

        public void SendMessage(string message, Chat chat)
        {
            chat.SendMessage(message);
        }

        //Overload function
        public void SendMessage(string message)
        {
            SendMessage(message, skypeChat);
        }
    }
}
