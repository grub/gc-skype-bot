﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Text.RegularExpressions;
using SKYPE4COMLib;

namespace GC_Skype_Bot
{
    public static class UserHandler
    {
        public static string path;                              //Path to save user data XML file to (will be set by the main class based on user parameter).
        public static string filename = "users.xml";            //Filename of user XML file.
        public static XmlDocument userXML = new XmlDocument();  //The main XML object, which users.xml file is loaded to.
        public static Dictionary<string, UserD> users = new Dictionary<string, UserD>();   //Users list (stores user handle and all the flags associated with the user).

        public static void Initialise(Chat chat)
        {
            bool madeChanges = false;

            Logging.AddToLog("Initialising user management...", false);

            //Try to load the XML file
            try
            {
                userXML.Load(UserHandler.path + UserHandler.filename);
            }
            catch(Exception e)
            {
                if (e.Message != "Root element is missing.")     //Only report an error if the exception isn't this, as this problem is fixed later in code
                {
                    Logging.AddToLog("  ERROR: Unable to load users.xml file, create it if it doesn't exist and ensure the path is valid.", false);
                    return;
                }
            }

            Logging.AddToLog("  Loaded users.xml file successfully from: '" + UserHandler.path + "'.", false);

            //Load users into users List variable from the XML file
            if (LoadUsersFromXML(userXML, out users) != null)
            {
                Logging.AddToLog("  Users loaded.", false);
            }
            //If the XML document lacks a root element, create it and import users from current chat
            else
            {
                XmlElement root = userXML.CreateElement("users");
                userXML.AppendChild(root);

                Logging.AddToLog("    Unable to find XML document root, it has been created.", false);

                AddFlagsToMembers(chat, new string[] { "user" });
                Logging.AddToLog("  No users were found in the XML document, so the users in the chat have been added.");
                madeChanges = true;
            }

            if (madeChanges)
            {
                //Save the XML if there were changes made.
                if (SaveXML())
                {
                    Logging.AddToLog("    Successfully saved users.xml.", false);
                }
                else
                {
                    Logging.AddToLog("    ERROR: Unable to save users.xml file, ensure the file is writable.", false);
                }
            }

            Logging.AddToLog("  Done.", false);
        }

        //Save the XML file
        public static bool SaveXML()
        {
            try
            {
                System.IO.Directory.CreateDirectory(path);  //Create the folder if it doesn't exist.
                userXML.Save(UserHandler.path + UserHandler.filename);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        //Loads users from the specified XML file and provides the list of users in the out parameter
        //Returns true on success, false if there were any invalid users (it is safe to resume from this), and null if unable to find document root or xml is blank
        private static bool? LoadUsersFromXML(XmlDocument xml, out Dictionary<string, UserD> userDictionary)
        {
            string handle;
            List<string> userFlags = new List<string>();
            XmlNodeList userFlagNodes;
            userDictionary = new Dictionary<string, UserD>();

            int faultyEntries = 0;

            if (xml.DocumentElement != null)    //Check if the XML is blank or invalid (lacking a root element)
            {
                //Finds all the <user> tags in the XML file, and adds the valid ones to the userDictionary.
                foreach (XmlNode userNode in xml.DocumentElement)
                {
                    handle = userNode.Attributes["handle"].InnerText;
                    userFlagNodes = userNode.SelectNodes("./flags/flag");

                    if (userFlagNodes.Count > 0)                  //Check that the <user> node has a <flags> node and that there are <flag> nodes
                    {
                        foreach (XmlNode flagNode in userFlagNodes)   //Add flags
                        {
                            userFlags.Add(flagNode.Attributes["id"].InnerText);
                        }
                    }

                    //Add the user to the user dictionary if the entry is valid.
                    if (!String.IsNullOrEmpty(handle))
                    {
                        new UserD(handle, userFlags.ToArray());
                        Logging.AddToLog("    Loaded user '" + handle + "' with " + userFlags.Count + " flags.", false);
                    }
                    //Otherwise, add to the tally of faulty entries.
                    else
                    {
                        faultyEntries++;
                    }

                    userFlags = new List<string>(); //Reset userFlags variable as it is used over and over in the loop.
                }

                if (faultyEntries == 0)
                {
                    return true;
                }

                //If there were any invalid entries, return false. Note that it is safe to resume from this as some invalid user entries are not a big deal in most cases.
                Logging.AddToLog("    ERROR: Unable to load " + faultyEntries + " invalid user entries.", false);
                return false;
            }

            //If the document element does not exist, then it ought to be created - return null.
            return null;
        }

        //Returns an XmlNode for a specified user handle. Will return null if the handle is not found in the XML.
        public static XmlNode GetUserNode(string handle)
        {
            XmlNode userNode = userXML.SelectNodes("//users/user[@handle='" + handle + "']")[0];
            return userNode;
        }

        //Adds flags to all members in the specified chat.
        public static void AddFlagsToMembers(Chat chat, string[] flags)
        {
            foreach (User member in chat.Members)
            {
                UserD user = GetUser(member.Handle) ?? new UserD(member.Handle, flags);
                user.AddFlags(flags);
            }
        }

        //Gets the UserD object from the user handle. Returns null if the UserD object does not exist in the dictionary.
        public static UserD GetUser(string handle)
        {
            UserD user;
            //Try to retrieve user from the dictionary...
            if (users.TryGetValue(handle, out user))
            {
                return user;
            }
            return null;
        }

        //Checks if a specified flag is valid (contains no special characters or spaces, and so on).
        public static bool IsFlagValid(string flag)
        {
            Regex regex = new Regex("^[A-Za-z]+$");

            if (!regex.IsMatch(flag))   //Ensure the flag is a single word with no special characters etc.
            {
                return true;
            }
            return false;
        }
    }

    public class UserD
    {
        public string Handle { get; private set; }      //The user handle (unique Skype username), can only be set when creating the UserD object.
        private List<string> Flags;                     //Flags, can be retrieved with GetFlags().
        public TempData tempData = new TempData();      //Temporary data storage for whatever needs it such as units.

        //Constructor.
        public UserD(string userHandle, string[] flags)
        {
            Handle = userHandle;
            Flags = new List<string>();
            AddFlags(flags);
        }

        //Gets flags into the specified list parameter, returns true if the flags were updated, returns false if they were not changed (they were the same).
        public bool GetUpdatedFlags(ref List<string> flags)
        {
            if (flags == Flags)
            {
                return false;
            }
            flags = Flags;
            return true;
        }

        //Returns the users flags in a list.
        public List<string> GetFlags()
        {
            return Flags;
        }

        //Adds a flag to a user.
        public void AddFlag(string flag)
        {
            string[] f = new string[] { flag };
            AddFlags(f);
        }

        //Adds the specified flags to a user and writes to the XML file the same changes.
        public void AddFlags(string[] flags)
        {
            XmlNode lastNode;
            lastNode = UserHandler.GetUserNode(Handle);

            //If the user node does not exist, create it.
            if (lastNode == null)
            {
                XmlElement userNode = UserHandler.userXML.CreateElement("user");
                userNode.SetAttribute("handle", Handle);
                lastNode = UserHandler.userXML.DocumentElement.PrependChild(userNode);

                XmlElement flagsNode = UserHandler.userXML.CreateElement("flags");
                lastNode = lastNode.PrependChild(flagsNode);
            }
            //Else, set the lastNode to the <flags> node (as lastNode ends up like this if it was created above) in preparation for adding the flags
            else
            {
                lastNode = lastNode.SelectNodes("./flags")[0];
            }

            //Set the flags in the user data object (or create it and add it to the dictionary if it doesn't exist)
            UserD user;
            if (UserHandler.users.TryGetValue(Handle, out user))
            {
                user.AddFlags(flags, false);
                UserHandler.users[Handle] = this;   //Not sure if this is necessary if out keyword passes by reference, but I will leave it for now.
            }
            else
            {
                UserHandler.users.Add(Handle, this);
            }

            //Add the flags to the XML document.
            foreach (string flag in flags)
            {
                //Check that the flag in the current iteration is not already in the XML file.
                bool alreadyInXML = false;
                if(lastNode.ChildNodes.Count > 0)
                {
                    foreach (XmlNode eFlagNode in lastNode.ChildNodes)
                    {
                        if (eFlagNode.Attributes != null && eFlagNode.Attributes["id"] != null)
                        {
                            if (eFlagNode.Attributes["id"].Value == flag)
                            {
                                alreadyInXML = true;
                                break;
                            }
                        }
                    }
                }
                //If the flag already exists in the XML, go to the next flag.
                if (alreadyInXML)
                {
                    continue;
                }

                XmlElement flagNode = UserHandler.userXML.CreateElement("flag");
                flagNode.SetAttribute("id", "user");
                lastNode.PrependChild(flagNode);
            }
        }

        //Overload function which can skip adding to XML if necessary.
        private void AddFlags(string[] flags, bool addToXML)
        {
            if (addToXML)
            {
                AddFlags(flags);
                return;
            }

            List<string> goodFlags = new List<string>();
            for (int i = 0; i < flags.Length; i++)
            {
                if (UserHandler.IsFlagValid(flags[i]))
                {
                    goodFlags.Add(flags[i]);
                }
            }

            Flags.AddRange(flags);
            return;
        }
    }

    public class TempData
    {
        private Dictionary<string, object[]> tempData = new Dictionary<string,object[]>();
        private DateTime lastCleanup;
        private TimeSpan maxAge = new TimeSpan(0, 30, 0);   //Any attempt to get some data after it has persisted for this amount of time will result in it being removed and not returned.
        
        //Accessors
        public TimeSpan Staleness
        {
            get { return DateTime.UtcNow - lastCleanup; }
        }

        public bool IsStale
        {
            get { return Staleness > maxAge; }
        }

        public int Count
        {
            get { return tempData.Count; }
        }


        public void Set(string name, string data)
        {
            tempData[name] = new object[2] { DateTime.UtcNow, data };
        }

        public string Get(string name, out DateTime storedUtc)
        {
            //Run a cleanup if it is due, alternatively, remove this data if it has expired and do not return it.
            if (IsStale)
            {
                Cleanup();
            }
            else
            {
                RemoveIfOld(name);
            }

            return GetCheap(name, out storedUtc);
        }

        private string GetCheap(string name, out DateTime storedUtc)
        {
            storedUtc = DateTime.MinValue;
            object[] dataObject;

            if (tempData.TryGetValue(name, out dataObject))
            {
                storedUtc = (DateTime)dataObject[0];
                return (string)dataObject[1];
            }
            return null;
        }

        //Overload
        public string Get(string name)
        {
            DateTime stored;
            return Get(name, out stored);
        }

        public bool Remove(string name)
        {
            return tempData.Remove(name);
        }

        public bool RemoveIfOld(string name)
        {
            DateTime storedUtc;
            GetCheap(name, out storedUtc);

            TimeSpan age = DateTime.UtcNow - storedUtc;

            if (age > maxAge)
            {
                Remove(name);
                return true;
            }
            return false;
        }

        public string Pop(string name, out DateTime storedUtc)
        {
            string data = Get(name, out storedUtc);

            if (data != null)
            {
                Remove(name);
                return data;
            }
            return null;
        }

        public void Cleanup()
        {
            lastCleanup = DateTime.UtcNow;

            foreach (KeyValuePair<string, object[]> entry in tempData)
            {
                RemoveIfOld(entry.Key);
            }
        }
    }
}
