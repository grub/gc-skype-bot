﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKYPE4COMLib;

namespace GC_Skype_Bot
{
    public class MessageHandler : gcMain
    {
        public Chat skypeChat;
        public List<units.Unit> listOfUnits;
        private bool initialised = false;

        public void Initialise()
        {
            if (!initialised)
            {
                //Keep track of all the units in a list (be sure to add new units to this list).
                listOfUnits = new List<units.Unit>()
                {
                    new units.RollTheDice(),
                    new units.Help(),
                    new units.Quit(),
                    new units.CommandList()
                };
                //Initialise all the units...
                foreach (units.Unit unit in listOfUnits)
                {
                    unit.InitUnit();
                }
            }
        }

        //Returns whether a specified user can execute a specified unit.
        public bool CanUserExec(UserD user, units.Unit unit, bool testTimeout = false)
        {
            List<string> userFlags = user.GetFlags();
            userFlags.Add("");

            //Check if the user has privilege to execute this unit before going any further...
            bool hasReq = false;
            foreach (string priv in userFlags)
            {
                if (unit.semantics.deniedPrivs.Contains<string>(priv))
                {
                    return false;
                }

                if (unit.semantics.requiredPriv == "" || unit.semantics.requiredPriv == priv)
                {
                    hasReq = true;
                }
            }

            return hasReq;
        }

        //Returns whether a specified user is on a timeout for using the specified command too fast.
        public bool IsUserOnTimeout(UserD user, units.Unit unit)
        {
            DateTime lastUse;
            TimeSpan timeoutLength = new TimeSpan(0, 0, unit.semantics.timeout);

            user.tempData.Get(unit.semantics.name + ": Last use", out lastUse);

            //If time now minus the time of last use is greater than the timeout length, then the user is not on timeout.
            if (DateTime.UtcNow - lastUse > timeoutLength)
            {
                return false;
            }
            return true;
        }

        //Sets a timeout on a user for a specified unit.
        public void SetTimeoutOnUser(UserD user, units.Unit unit)
        {
            user.tempData.Set(unit.semantics.name + ": Last use", "");
        }

        public void OnReceiveMessage(ChatMessage msg, TChatMessageStatus status)
        {
            //Ensure the message is from the appropriate chat...
            if (status == TChatMessageStatus.cmsReceived && msg.Chat.Blob == skypeChat.Blob)
            {
                //Test the message against all of the units triggers...
                foreach (var unit in listOfUnits)
                {
                    //Get the user who sent the message.
                    UserD user = UserHandler.GetUser(msg.Sender.Handle);

                    //If the user does not exist in the user dictionary, then track this new user...
                    if (user == null)
                    {
                        user = new UserD(msg.Sender.Handle, new string[] { "user" });
                        Logging.AddToLog("Tracked new user: '" + user.Handle + "'.");
                    }

                    if (!unit.semantics.enabled)
                    {
                        continue;   //If the unit is disabled, skip this iteration and go on to the next.
                    }

                    int triggerPos = msg.Body.IndexOf(unit.semantics.triggerPre + unit.semantics.trigger); //Get the position of the trigger in the message body, if possible.

                    //If the trigger is found, we may have a match...
                    if (triggerPos >= 0)
                    {
                        //If the unit trigger has to be first-thing in the message, and it is the first thing, we have a match!
                        //Otherwise, if the unit does not need to be triggered first-thing, then it's a match!
                        if ((unit.semantics.firstChar && triggerPos == 0) || !unit.semantics.firstChar)
                        {
                            Logging.AddToLog("Unit '" + unit.semantics.name + "' triggered by '" + user.Handle + "'.");

                            //Check the user has the appropriate privileges to execute this command...
                            if (CanUserExec(user, unit))
                            {
                                //Finally ensure that the user has waited long enough to execute this command...
                                if (!IsUserOnTimeout(user, unit))
                                {
                                    //At this point, hopefully everything went smoothly and the user has good intentions, execute the command!
                                    unit.DoCommand(user, msg);

                                    SetTimeoutOnUser(user, unit);
                                }
                                else
                                {
                                    //If the user was on a timeout, don't execute the unit and print a message to the log.
                                    Logging.AddToLog("  Unit was not executed: user was on a timeout for this command.");
                                }
                            }
                            //If the user does not have appropriate privileges, then print to log and send a message...
                            else
                            {
                                skypeHandler.SendMessage(user.Handle + ": You do not have permission to perform this action.");
                                Logging.AddToLog("  Unit was not executed: insufficient user permissions.");
                            }
                        }
                    }
                }
            }
        }
    }
}
