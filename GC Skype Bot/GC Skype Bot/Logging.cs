﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GC_Skype_Bot
{
    public static class Logging
    {
        private static DateTime epochDate = new DateTime(2012, 1, 1);   //Arbitrary date to timestamp logs uniquely.
        private static DateTime today = DateTime.Today.AddDays(-1);     //Holds the date when AddToLog was last used (default value is yesterday so that AddToLog prints date first-run).
        private static Queue<string> log = new Queue<string>(1000);     //Queue to hold the log.
        public static string path;                                      //Path to save log to (will be set by the main class based on user parameter).
        private static string fileName = "";                            //Filename of the log, gets set later on in the script to include a timestamp.

        public static void AddToLog(string text, bool includeTime)
        {
            string time = "";

            //Gets the time if the includeTime boolean is true, so that it can be added at the beginning of the line.
            if (includeTime)
            {
                time = "[" + DateTime.Now.ToString("HH:mm:ss") + "] ";  //The time, represented like: [19:43] 
            }

            //Prints the date if it is no longer the same day as when this function was last run.
            if (today.Date != DateTime.Today.Date)
            {
                today = DateTime.Today;     //Sets the variable to today so that this function does not run again until tomorrow.
                Console.WriteLine(today.ToString("dddd, MMMM d, yyyy") + ".");   //Prints the date, for instance: Wednesday, July 16, 2014.
            }

            //Include the log text to the log queue, and print it.
            string logLine = includeTime ? time + text : text;  //The log line, includes the time if includeTime is ommited or specified true.
            log.Enqueue(logLine);
            Console.WriteLine(logLine);

            //Every 20 lines of logging, save it to file automatically.
            if (log.Count % 20 == 0)
            {
                SaveLog();
            }
        }

        //Overload function
        public static void AddToLog(string text)
        {
            AddToLog(text, true);
        }

        public static bool SaveLog()
        {
            //If filename is blank, set it to gc-log_{timestamp}.txt.
            if(fileName == "")
            {
                TimeSpan sinceEpoch = DateTime.UtcNow - epochDate;
                fileName = "gc-log_" + (int)sinceEpoch.TotalSeconds + ".txt"; //File name (utilises timestamp)
            }

            //There is always the possibility that the directory may become unavailable to write to after the application is running, so use a try-catch to write the file.
            try
            {
                System.IO.Directory.CreateDirectory(path);  //Create the folder if it doesn't exist.
                System.IO.File.WriteAllLines(path + fileName, log.ToArray());
            }
            catch(Exception e)
            {
                AddToLog("ERROR: Unable to write log to file. Please ensure path is valid and is writable.");
                return false;
            }

            return true;
        }
    }
}
